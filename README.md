# README #

This repo contain useful helpers to speed up the basic development.

### What is this repository for? ###

* Initially, I have added console command to create custom field templates.
* Version 1.0

### How do I get set up? ###

* Copy files from helper directory to your sugar under the same path
* Once copied, perform Quick Repair & Rebuild
* run php bin\sugarcrm list and you will able to see hats:mimic:field command, it accepts 3 arguments - type, module, fieldname
* run php bin\sugarcrm hats:mimic:field int Accounts employee_count