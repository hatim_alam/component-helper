<?php

/** Written by: hatim.alam@izeno.com **/

namespace Sugarcrm\Sugarcrm\custom\Library;
use RuntimeException;

class FieldTypeTemplate
{
    protected $field_type;
    protected $module;
    protected $field_name;
    protected $common_field_properties = array();

    public function __construct()
    {
        $this->common_field_properties = array(
            'required' => false,
            'audited' => false,
            'reportable' => true,
            'importable' => true,
            'mass_updated' => true,
            'calculated' => false,
        );
    }

    public function setFieldProperties($field_type='text', $module='Accounts', $field_name='test_field')
    {
        //if field type is empty, fallback to textfield template
        $this->field_type = $field_type;
        //if module is empty, defaults to Accounts module
        $this->module = $module;
        //if field name is empty, defaults to test_field_$module
        $this->field_name = $field_name;

        return $this;
    }

    public function createField()
    {
        global $beanList;
        if(empty($this->field_type) || empty($this->module) || empty($this->field_name))
        {
            $this->setFieldProperties();
        }
        require_once("include/upload_file.php");
        $template_name = $this->field_type."Template";
        $field_defs = $this->$template_name();
        $field_defs = array_merge($field_defs, $this->common_field_properties);
        $path = "custom/Extension/modules/{$this->module}/Ext/Vardefs/sugarfield_{$this->field_name}.php";
        $bean_name = ($beanList[$this->module] == 'aCase') ? "Case" : $beanList[$this->module];
        $out = "<?php\n //created through script : ".date('Y-m-d H:i:s'). "\n";
        foreach($field_defs as $property => $value)
        {
            $out .= override_value_to_string_recursive(array($bean_name, "fields", $this->field_name, $property), "dictionary", $value). "\n";
        }
        $out .= "\n ?>";
        if(!file_exists($path))
        {
            if(@sugar_touch($path))
            {
                @sugar_file_put_contents($path, $out);
                return true;
            } else {
                throw new RuntimeException("Error: Cannot create vardef file: $path. Please check directory permissions.");
                return false;
            }
        } else {
            throw new RuntimeException("Error: Cannot create new field, field already exists.");
            return false;
        }
    }

    private function intTemplate()
    {
        $field['name'] = $this->field_name;
        $field['vname'] = "LBL_".strtoupper($this->field_name);
        $field['type'] = 'int';
        $field['len'] = 10;
        $field['comments'] = 'Custom integer field';
        $field['min'] = 0;
        $field['max'] = 100;
        return $field;
    }

    private function textTemplate()
    {
        $field['name'] = $this->field_name;
        $field['vname'] = "LBL_".strtoupper($this->field_name);
        $field['type'] = 'varchar';
        $field['len'] = 30;
        $field['comments'] = 'Custom text field';
        return $field;
    }

    private function enumTemplate()
    {
        $field['name'] = $this->field_name;
        $field['vname'] = "LBL_".strtoupper($this->field_name);
        $field['type'] = 'enum';
        $field['dbType'] = 'varchar';
        $field['len'] = 30;
        $field['comments'] = 'Custom text field';
        $field['options'] = $this->field_name."_dom";
        return $field;
    }
}
