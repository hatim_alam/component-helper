<?php

/** Written by: hatim.alam@izeno.com **/

namespace Sugarcrm\Sugarcrm\custom\Console\Command;

use Sugarcrm\Sugarcrm\Console\CommandRegistry\Mode\InstanceModeInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Sugarcrm\Sugarcrm\custom\Library\FieldTypeTemplate;
use RuntimeException;

class MimicComponentCommand extends Command implements InstanceModeInterface
{
    protected function configure()
    {
        $this
            ->setName('hats:mimic:field')
            ->addArgument('type', InputArgument::REQUIRED, 'Valid sugar field type - int, enum, text, textarea, multienum, decimal and currency')
            ->addArgument('module', InputArgument::REQUIRED, 'Module name (i.e. Accounts etc.) where you want to create this custom field')
            ->addArgument('fieldname', InputArgument::REQUIRED, 'Your custom field name')
            ->setDescription('Quick create custom field definitions in extension directory.')
            ->setHelp('This command helps quickly creating basic custom field definition in extension directory.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        global $beanList;
        $field_type = $input->getArgument('type');
        $module_name = $input->getArgument('module');
        $field_name = $input->getArgument('fieldname');

        $supported_field_types = array('int', 'enum', 'text', 'multienum', 'decimal', 'currency');
        if(!in_array($field_type, $supported_field_types))
        {
            throw new RuntimeException("Invalid type $field_type. Please refer help documentation for supported field types.");
        }
        if(!isset($beanList[$module_name]))
        {
            throw new RuntimeException("No such module $module_name exists in your application.");
        }
        //fetch appropriate field type template
        $field_tempalte_obj = new FieldTypeTemplate();
        $result = $field_tempalte_obj->setFieldProperties($field_type, $module_name, $field_name)->createField();
        if($result)
        {
            $output->writeln("Success: Field created successfully.");
        }
    }
}

?>